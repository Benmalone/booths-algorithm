#include "booth.h"
#include "math.h"
#include <iostream>
#include <bitset>

using namespace std;

Booth::Booth(int uMcand, int uMplier)
{
	bitset<8> bCand = uMcand;//A will hold the binary representation of N 
	bitset<8> bPlier = uMplier;
	multiplicand = uMcand;
	multiplier = uMplier;
	for (int i = 0, j = 7; i<8; i++, j--)
	{
		//Assigning the bits one by one.
		mCand[i] = bCand[j];
		mPlier[i] = bPlier[j];
	}
    mCand[8] = 0;
	mPlier[8] = 0;
	for (int i = 0; i < 17; i++)
	{
		if (i < 8)
		{
			prod[i] = 0;
		}
		else
		{
			prod[i] = mPlier[i - 8];
		}
	}
}

void Booth::add()
{
	int temp[8];
	for (int i = 0; i < 8; i++)
	{
		temp[i] = prod[i] + mCand[i];
	}
	for (int i = 7; i >= 0; i--)
	{
		if (temp[i] == 2)
		{
			temp[i] = 0;
			if (i != 0)
			{
				temp[i - 1] += 1;
			}
		}
		if (temp[i] == 3)
		{
			temp[i] = 1;
			if (i != 0)
			{
				temp[i - 1] += 1;
			}
		}
	}
	for (int i = 0; i < 8; i++)
	{
		prod[i] = temp[i];
	}
}

void Booth::sub()
{
	int temp[8];
	int negM[8];
	bitset<8> negative = (-1 * multiplicand);
	for (int i = 0, j = 7; i<8; i++, j--)
	{
		//Assigning the bits one by one.
		negM[i] = negative[j];
	}
	for (int i = 0; i < 8; i++)
	{
		temp[i] = prod[i] + negM[i];
	}
	for (int i = 7; i >= 0; i--)
	{
		if (temp[i] == 2)
		{
			temp[i] = 0;
			if (i != 0)
			{
				temp[i - 1] += 1;
			}
		}
		if (temp[i] == 3)
		{
			temp[i] = 1;
			if (i != 0)
			{
				temp[i - 1] += 1;
			}
		}
	}
	for (int i = 0; i < 8; i++)
	{
		prod[i] = temp[i];
	}
}

void Booth::prodShift()
{
	for (int i = 16; i > 0; i--)
	{
		prod[i] = prod[i - 1];
	}
}

void Booth::print()
{
	cout << "Iteration |          Step         |  Mcand   | Product" << endl;
	cout << "----------------------------------------------------------------" << endl;
	cout << "    0     |          Init         | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << endl;
	cout << "----------------------------------------------------------------" << endl;
	cout << "    1     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    2     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    3     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    4     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    5     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    6     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    7     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n----------------------------------------------------------------" << endl;
	cout << "    8     | ";
	if ((prod[15] == 0 && prod[16] == 0) || (prod[15] == 1 && prod[16] == 1))
	{
		cout << "        No OP         | ";
	}
	else if (prod[15] == 0 && prod[16] == 1)
	{
		cout << "Prod' = Prod' + Mcand | ";
		add();
	}
	else if (prod[15] == 1 && prod[16] == 0)
	{
		cout << "Prod' = Prod' - Mcand | ";
		sub();
	}
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n          |      Prod ASR >> 1    | ";
	for (int i = 0; i < 8; i++)
	{
		cout << mCand[i];
	}
	cout << " | ";
	prodShift();
	for (int i = 0; i < 17; i++)
	{
		cout << prod[i];
	}
	cout << "\n\n" << multiplicand << " * " << multiplier << " = " << multiplicand * multiplier << endl;
}