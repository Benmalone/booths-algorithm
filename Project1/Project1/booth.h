#ifndef BOOTH_H
#define BOOTH_H
#include "string.h"

/*namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};*/

class Booth
{
private:
    int mCand[9];
    int mPlier[9];
	int prod[17];
	int multiplicand;
	int multiplier;
public:
    Booth(int uMcand, int uMplier);
    void print();
    void add();
    void sub();
    void prodShift();
};

#endif BOOTH_H
