#include "booth.h"
#include <iostream>
#include <stdio.h>
#include <bitset>
#include <math.h>

using namespace std;

int main(int argc, char *argv[])
{
	int value1;
	int value2;
	bool run = true;
	while (run)
	{
		bool valid = false;
		while (!valid)
		{
			valid = true;
			cout << "Enter a value: " << endl;
			cin >> value1;
			cout << "Enter another value: " << endl;
			cin >> value2;
	
			if (cin.fail() || abs(value1) > 128 || abs(value2) > 128)
			{
				cin.clear();
				cin.ignore();
				cout << "Please enter a valid number between 128 and -128.\n";
				valid = false;
			}
		}
		Booth booth(value1, value2);
		booth.print();
		cout << "\nRun Again? Y/N" << endl;
		string confirm;
		cin >> confirm;
		if (confirm == "N" || confirm == "n" || confirm == "no" || confirm == "No" || confirm == "NO")
		{
			run = false;
		}
	}
	system("pause");
    return 0;
}
